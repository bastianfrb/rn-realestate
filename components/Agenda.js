import React, { Component } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Dimensions } from 'react-native'
import t from 'tcomb-form-native'

const Form = t.form.Form

const Persona = t.struct({
    rut: t.String,
    nombre: t.String,
    teléfono: t.Number,
    correo: t.String,
    fecha: t.String,
})

const opt = {}

export default class Agenda extends Component {
    static navigationOptions =  {
        title: 'Agendar Visita'
    }

    constructor(props){
        super(props)

        this.state = {
            propiedad: this.props.navigation.state.params.propiedad
        }
    }

    publicar(){
        const values = this.refs.form.getValue()
        if(values)
            console.warn('FORM VALUES', values)
    }

    render() {
        return (
            <View style={styles.container}>
                <Form 
                    ref='form'
                    type={Persona}
                    options={opt}
                />
                <TouchableOpacity style={styles.button} onPress={this.publicar} underlayColor='#99d9f4'>
                    <Text style={styles.buttonText}>Agendar</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const win = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        top: 15,
        width: win.width,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 20
    },
    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        height: 36,
        backgroundColor: '#48BBEC',
        borderColor: '#48BBEC',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        alignSelf: 'stretch',
        justifyContent: 'center',
        width: (win.width/2),
        left: (win.width/4)
    }
})