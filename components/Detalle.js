import React, { Component } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image, Dimensions, Button, ScrollView } from 'react-native'

export default class Detalle extends Component {
    static navigationOptions =  {
        title: 'Detalle de Propiedad'
    }

    constructor(props){
        super(props)

        this.state = {
            tituloAnterior: this.props.navigation.state.params.tituloAnterior,
            item: this.props.navigation.state.params.item
        }
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <ScrollView style={styles.container}>
                <ScrollView>
                <Image
                        style={styles.imagen} 
                        source={{uri: 'https://pbs.twimg.com/profile_images/622768643504517120/8gm4FQAc.jpg'}} 
                />
                <View style={styles.detail}>
                    <Text>{this.state.item.registered}</Text>
                    <Text style={styles.title}>{this.state.item.name}</Text>
                </View>

                <View style={styles.itemDetail}>
                    <Text style={styles.itemLabel}>Mt Construídos <Text style={styles.textItem}> 23 mt2</Text></Text>
                    <Text style={styles.itemLabel}>Superficie Total <Text style={styles.textItem}>102 mt2</Text></Text>
                    <Text style={styles.itemLabel}>Dormitorios <Text style={styles.textItem}>3</Text></Text>
                    <Text style={styles.itemLabel}>Baños <Text style={styles.textItem}>2</Text></Text>
                </View>

                <Text style={styles.descripcion}>{this.state.item.about}</Text>
                </ScrollView>
                <Button 
                    style={styles.boton}
                    onPress={() => navigate('Agenda', {propiedad: this.state.item})}
                    title="Agendar Visita"
                />
            </ScrollView>
        )
    }
}

const win = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    detail: {
        backgroundColor: '#E4E4E4',
        flex: 0,
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 20
    },
    imagen: {
        width: win.width,
        height: 200,
    },
    descripcion: {
        flex: 1,
        left: 15,
        right: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },
    boton: { 
        height: 50, 
        justifyContent: 'center', 
        alignItems: 'center',
        position: 'relative',
        bottom: 0,
        width: win.width,
    },
    textItem:{
        flex: 0,
        color: 'black',
        textAlign: 'right',
    },
    itemLabel: {
        left: 15,
        right: 15,
        height: 40,
        top: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemDetail: {
        backgroundColor: '#F4F4F4',
        flex: 0,
    }
})