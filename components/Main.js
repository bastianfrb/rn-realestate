import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { StackNavigator } from 'react-navigation'
import Propiedad from './Propiedad'
import Detalle from './Detalle'
import Agenda from './Agenda'

/* En el stack navigator, el primer ítem, será el que se muestre cuando inicie este js */
const Screens = StackNavigator({ 
    Propiedad: {screen: Propiedad},
    Detalle: {screen: Detalle},
    Agenda: {screen: Agenda},
})

export default Screens