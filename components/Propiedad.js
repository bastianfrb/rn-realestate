import React, { Component } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, FlatList, Image, Dimensions } from 'react-native'



export default class Propiedad extends Component {
    static navigationOptions =  {
        title: 'Listado de Propiedades',
    }
    
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            data: null,
        }
    }

    
    renderItem = ({ item }) => {
        const { navigate } = this.props.navigation
        return (
            <TouchableOpacity onPress={() => navigate('Detalle', {item: item})}>
                <Image
                        style={styles.imagen}
                        source={{uri: 'https://pbs.twimg.com/profile_images/622768643504517120/8gm4FQAc.jpg'}} 
                />    
                <View style={styles.textRegistro}>
                    <Text style={styles.titulo}> {item.name} </Text>
                    <Text style={styles.precio}> {item.balance} </Text>
                </View>
            </TouchableOpacity>
        )
    }

    componentDidMount(){
        return fetch('http://www.json-generator.com/api/json/get/cpWyQxFUPm?indent=2')
        .then(res => res.json())
        .then(resjson => {
            this.setState({
                isLoading: false,
                data: resjson,
            })
            
        })
        .catch((err) => {
            console.error(err)
        })
    }

    render() {
        if(this.state.isLoading){
            return(
                <View style={[styles.container, styles.activity]}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
        }else{
            return(
                <View style={styles.container}>
                    <FlatList
                        data = {this.state.data}
                        renderItem = {this.renderItem}
                        keyExtractor = {(item, index) => index}
                    />
                </View>
            )
        }
    }
}

const win = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flex: 0,
    },
    textRegistro: {
        flexDirection: 'row',
        flex: 1,
        position: 'absolute',
        top: 100
    },
    titulo: {
        fontSize: 24
        , color: 'white'
        , flex: 1
        , top: 30
        , fontWeight: 'bold'
        , textShadowRadius: 10
        , textShadowColor: 'black'

    },
    precio: {
        flex: 0, 
        flexDirection: 'row', 
        marginBottom: 3,
        top: 70,
        fontSize: 20,
        textShadowRadius: 5,
        textShadowColor: 'black',
    },
    imagen: {
        flex: 1,
        width: win.width,
        height: 200
    },
    activity: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10,
    }
})